package crawlers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class IabiletCrawler implements Runnable {
    public void run() {

        List<Event> events = new ArrayList<Event>();

        Document doc = null;
        try {
            doc = Jsoup.connect("https://www.iabilet.ro").get();
            Elements locations = doc.select(".location-menu .dropdown-menu-left li a");
            for (Element e : locations) {
                String href = e.attr("href");
                String urlLocation = "https://www.iabilet.ro" + href + "?eventListPaginatedId=eventListPaginatedId";

                int page = 1;
                while (true) {
                    String urlLocationPaginated = urlLocation + "&page=" + page;
                    Document documentLocationPaginated = Jsoup.connect(urlLocationPaginated).get();
                    Elements eventsEl = documentLocationPaginated.select(".event-list .event-list-item");
                    if (eventsEl.isEmpty()) {
                        break;
                    }
                    for (Element eventEl : eventsEl) {
                        String eventLocation = eventEl.select(".location a span").text();
                        String eventName = eventEl.select(".title span strong").text();
                        String url = eventEl.select(".title a").attr("href");
                        String desc = eventEl.select(".main-info div:nth-child(3)").text();
                        String price = eventEl.select(".price").text();
                        String dateDay = eventEl.select(".date-start .date-day").text();
                        String dateMonth = eventEl.select(".date-start .date-month").text();

                        Date date = new GregorianCalendar(2018, getMonthIndex(dateMonth),
                                Integer.parseInt(dateDay) - 1).getTime();

                        Event event = new Event();

                        event.setName(eventName);
                        event.setLocation(eventLocation);
                        event.setUrl("https://www.iabilet.ro" + url);
                        event.setDesc(desc);
                        event.setPrice(price);
                        event.setDate(date);
                        events.add(event);
                    }
                    page++;
                }

            }

            System.out.println(events.size());

            System.out.println(events.get(10));

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private int getMonthIndex(String month) {
        switch (month) {
            case "ian":
                return 0;

            case "feb":
                return 1;

            case "mar":
                return 2;

            case "apr":
                return 3;

            case "mai":
                return 4;

            case "iun":
                return 5;

            case "iul":
                return 6;

            case "aug":
                return 7;

            case "sep":
                return 8;

            case "oct":
                return 9;

            case "noi":
                return 10;

            case "dec":
                return 11;

        }
        return 0;
    }
}
